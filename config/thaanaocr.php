<?php

return [

    'vision_api' => [
        'enabled' => env('VISION_API_ENABLED', true),
        'url'     => env('VISION_API_URL'),
        'key'     => env('VISION_API_KEY'), //AIzaSyDmnFZnWe9U_-4MrnHPidtaxw4pQKvunks
        'language_hints' => ['dv']
    ],

];
