<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Vision\V1\AnnotateFileResponse;
use Google\Cloud\Vision\V1\AsyncAnnotateFileRequest;
use Google\Cloud\Vision\V1\Feature;
use Google\Cloud\Vision\V1\Feature\Type;
use Google\Cloud\Vision\V1\GcsDestination;
use Google\Cloud\Vision\V1\GcsSource;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Google\Cloud\Vision\V1\InputConfig;
use Google\Cloud\Vision\V1\OutputConfig;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;

class PdfController extends Controller
{
    public function uploadPdfToGCS(Request $request) {
    	$request->validate([
          'file' => 'required|mimes:pdf|max:10240', 
      	]);

        // if ($path = $request->file('file')->store('/uploaded/ocr/pdfs')) {
            // $pdfFile = storage_path('app/' . $path);

            $originalFileName = pathinfo($request->file('file')->getClientOriginalName())['filename'];

            $file = $request->file('file');

            $disk = Storage::disk('uploads');
            // $path = $request->file('file')->store('/uploaded/ocr/pdfs');
			$path = $disk->put('/ocr/pdfs', $file);

            $pdfFile = storage_path('app/' . $path);
			$localFileName = pathinfo($pdfFile)['filename'];



            $disk = Storage::disk('gcs');


		// create a file
			$disk->put($originalFileName, $file);

            $fileName = $file->hashName();

            // var_dump($file->hashName());

		// }

            $path = 'gs://thaanaocr/' . $originalFileName . '/' . $fileName;

            return $this->detect_pdf_gcs($path, $originalFileName, $fileName, $localFileName);
    }

	// $path = 'gs://path/to/your/document.pdf'
	// $output = 'gs://path/to/store/results/'

	public function detect_pdf_gcs($path, $originalFileName, $fileName, $localFileName)
	{

		// $path = 'gs://thaanaocr/2017-1-O-7-4.pdf';
		$output = 'gs://thaanaocr/extracts/';

		// file name and folder name to create
        // $fileNameString = Str::random(10);

        $output .= $originalFileName . '/';

	    # select ocr feature
	    $feature = (new Feature())
	        ->setType(Type::DOCUMENT_TEXT_DETECTION);

	    # set $path (file to OCR) as source
	    $gcsSource = (new GcsSource())
	        ->setUri($path);

	    # supported mime_types are: 'application/pdf' and 'image/tiff'
	    $mimeType = 'application/pdf';
	    $inputConfig = (new InputConfig())
	        ->setGcsSource($gcsSource)
	        ->setMimeType($mimeType);

	    # set $output as destination
	    $gcsDestination = (new GcsDestination())
	        ->setUri($output);
	    # how many pages should be grouped into each json output file.


	    $batchSize = 1;
	    $outputConfig = (new OutputConfig())
	        ->setGcsDestination($gcsDestination);
	        // ->setBatchSize($batchSize);

	    # prepare request using configs set above
	    $request = (new AsyncAnnotateFileRequest())
	        ->setFeatures([$feature])
	        ->setInputConfig($inputConfig)
	        ->setOutputConfig($outputConfig);
	    $requests = [$request];



	    # make request
	    $imageAnnotator = new ImageAnnotatorClient();
	    $operation = $imageAnnotator->asyncBatchAnnotateFiles($requests);
	    // print('Waiting for operation to finish.' . PHP_EOL);
	    $operation->pollUntilComplete();


	    # once the request has completed and the output has been
	    # written to GCS, we can list all the output files.
	    preg_match('/^gs:\/\/([a-zA-Z0-9\._\-]+)\/?(\S+)?$/', $output, $match);
	    $bucketName = $match[1];
	    $prefix = isset($match[2]) ? $match[2] : '';

	    // $prefix = isset($match[3]) ? $prefix . '/' .$match[3] : $prefix;

	    $storage = new StorageClient([
		    'keyFile' => json_decode(file_get_contents(base_path() . '\vision-key.json'), true)
		]);
	    $bucket = $storage->bucket($bucketName);
	    $options = ['prefix' => $prefix];
	    $objects = $bucket->objects($options);


	    # save first object for sample below
	    $objects->next();
	    $firstObject = $objects->current();

	    # list objects with the given prefix.
	    // print('Output files:' . PHP_EOL);
	     $text = '';
	    foreach ($objects as $k => $object) {
	    		// print($object->name() . PHP_EOL);
	    		// echo '<br/>';


	    	// the first object always returns the parent folder 
	    	// if ($k>0) {
	    		// print($object->name() . PHP_EOL);
	        $json = $object->downloadAsString();
	        $batch = new AnnotateFileResponse();
	        $batch->mergeFromJsonString($json);

	        foreach ($batch->getResponses() as $response) {
		        $annotation = $response->getFullTextAnnotation();
		        // print($annotation ? $annotation->getText() : '');
		        if ($annotation) {
		        	$text .= $annotation->getText();
		        }
		    }
	    	// }
	        
	    }

	    # process the first output file from GCS.
	    # since we specified batch_size=2, the first response contains
	    # the first two pages of the input file.
	    // $jsonString = $firstObject->downloadAsString();
	    // $firstBatch = new AnnotateFileResponse();
	    // $firstBatch->mergeFromJsonString($jsonString);

	    // # get annotation and print text
	    // $text = '';
	    // foreach ($firstBatch->getResponses() as $response) {
	    //     $annotation = $response->getFullTextAnnotation();
	    //     // print($annotation ? $annotation->getText() : '');
	    //     if ($annotation) {
	    //     	$text .= $annotation->getText();
	    //     }
	    // }

	    


	    $imageAnnotator->close();

	    // return $text;

	    // return response()->json([
     //            'text' => $text
     //        ]);

	    return response()->json([
                // 'pages' => $pages,
                'text' => $text,
                'file' => $localFileName,
                'originalFileName' => $originalFileName
            ]);
	}
}
