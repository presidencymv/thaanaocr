<?php

namespace App\Http\Controllers;

use App\ThaanaOCR;
use App\Http\Requests\OCR\ImageFileUploadRequest;
use Illuminate\Http\Request;

use Spatie\PdfToImage\Pdf;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;

use _PDF;

// disable require tcpdf line from vendor/daltcore/lara-pdf-merger/src/Lynx39/LaraPdfMerger/PdfManage.php -- line 8
use PDFMerger;

use App\Models\VisionRequest;

class ThaanaOCRController extends Controller
{
    public function __construct(ThaanaOCR $ocr)
    {
        $this->ocr = $ocr;
    }

    public function index(Request $request) {

        // if (!extension_loaded('imagick')) {
        //     die('Imagick not loaded. It is a required package for PDF extraction.');
        // }

        $count = VisionRequest::where('id', '>', 0)->get()->count();

        $data = [
            'count' => $count
        ];

        return view('app')->with($data);
    }


    public function uploadPhoto(Request $request)
    {

        $request->validate([
          'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',
      ]);

        $disk = Storage::disk('uploads');

        // if ($path = $request->file('file')->store('/uploaded/ocr/images')) {
        if($path = $disk->put('/ocr/images', $request->file('file'))) {

            // var_dump($path);
            // logger(storage_path('app/' . $path));

            $path = 'app/uploaded/' . $path;

            $string = $this->ocr->fromImage(storage_path($path))->recognise()->getText();

            // $string = preg_replace("/\r?\n|\r/", " ", $string);

            // delete uploaded image
            unlink(storage_path($path));

            $this->logRequest($request);

            return response()->json([
                'text' => $string
            ]);
        }
    }

    public function uploadPdf(Request $request)
    {

        $request->validate([
          'file' => 'required|mimes:pdf|max:10240', 
      ]);

        if ($path = $request->file('file')->store('/uploaded/ocr/pdfs')) {
            $pdfFile = storage_path('app/' . $path);

        $originalFileName = pathinfo($request->file('file')->getClientOriginalName())['filename'];//pathinfo($pdfFile)['filename'];


            $result = $this->pdfToImages($pdfFile);
            $fileName = pathinfo($pdfFile)['filename'];

            logger($pdfFile);
            // echo $originalFileName;

            $pages = [];

            $imageFiles = $result['imageFiles'];

            foreach ($imageFiles as $key => $image) {
                $pages[] = [
                    'page_number' => $key + 1,
                    'text' => $this->ocr->fromImage($image)->recognise()->getText()
                ];

                $this->logRequest($request);

                // delete image file
                unlink($image);
            }

            // delete pdf file
            // unlink($pdfFile);

            // delete directory we created for this upload
            rmdir($result['folderPath']);

            $text = '';
            foreach ($pages as $key => $p) {
                $text .= $p['text'];
                
            }

            return response()->json([
                'pages' => $pages,
                'text' => $text,
                'file' => $fileName,
                'originalFileName' => $originalFileName
            ]);
        }
    }

    public function downloadPdf(Request $request) {

        $text = $request->input('text');
        $pages = $request->input('pages');
        $fileName = $request->input('file');
        $originalFileName = $request->input('originalFileName');

        $path = storage_path('app/uploaded/ocr/pdfs');
        

        _PDF::SetTitle('OCR Text Merger');

        _PDF::SetTextColor(0, 0, 0);

        _PDF::AddPage();

        _PDF::SetRTL(true);

        _PDF::SetFont('mvfaseyha', '', 11);
        // _PDF::SetTextColor(0, 0, 0, 0);
        // _PDF::SetFillColor(255, 255, 200);
        // _PDF::SetTextColor(255,255,255);


        _PDF::Write(0,$text);


        $newPdf = $path . '/' . $fileName . '-ocr.pdf';
        _PDF::Output($newPdf, 'F');


        $originalUrl = 'ocr'.DIRECTORY_SEPARATOR.'pdfs'.DIRECTORY_SEPARATOR.$fileName.'.pdf';
        $ocrUrl = 'ocr'.DIRECTORY_SEPARATOR.'pdfs'.DIRECTORY_SEPARATOR.$fileName.'-ocr.pdf';

        $origianlFilePath = storage_path('app'.DIRECTORY_SEPARATOR.'uploaded'.DIRECTORY_SEPARATOR.$originalUrl);
        $ocrFilePath = storage_path('app'.DIRECTORY_SEPARATOR.'uploaded'.DIRECTORY_SEPARATOR.$ocrUrl);


        $mergeFileName = $originalFileName;//Str::random(10);
        $mergePdfFile = $mergeFileName . '-D.pdf';
        $mergeUrl = 'ocr'.DIRECTORY_SEPARATOR.'pdfs'.DIRECTORY_SEPARATOR.$mergePdfFile;
        $mergeFilePath = storage_path('app'.DIRECTORY_SEPARATOR.'uploaded'.DIRECTORY_SEPARATOR.$mergeUrl);


        // merge both pdfs
        $pdfMerger = PDFMerger::init();
        $pdfMerger->addPDF(($origianlFilePath), 'all');
        $pdfMerger->addPDF(($ocrFilePath), 'all');
        $pdfMerger->merge();

        $pdfMerger->save($mergeFilePath, "download");

        // delete pdf files
        unlink($origianlFilePath);
        unlink($ocrFilePath);

//      $fs = new Filesystem();
//      $name = 'uploads.'.$fs->extension($mergeFilePath);
//      return response()->download($mergeFilePath, $name);

    }

    protected function pdfToImages($pdfFile)
    {

        $fileName = pathinfo($pdfFile)['filename'];
        $pdfPath = storage_path('app/uploaded/ocr/pdfs/');

        $pdf = new \Spatie\PdfToImage\Pdf($pdfFile);
        $numberOfPages = $pdf->getNumberOfPages();

        $imageFiles = [];

        // file name and folder name to create
        $fileNameString = Str::random(10);

        // file name
        $fileName = $fileNameString . '.png';

        // converted images to be stored in this path
        $convertedImagesPath = $pdfPath . 'converted_images';

        $uploadPath = $convertedImagesPath . '/' . $fileNameString;

        // create directory for this upload
        File::makeDirectory($uploadPath);

        // create image(s) from the pdf file
        exec('magick convert ' . $pdfFile . ' -quality 500 ' . $uploadPath . '/' . $fileName);

        // get all files in the directory we created above
        $files = scandir($uploadPath);

        foreach($files as $k => $f) {
            if (strlen($f) < 10) {
                unset($files[$k]);
            } else {
                $file = $uploadPath . '/' . $f;
                $imageFiles[] = $file;
            }
        }

        // apply natural order sort for the file names
        natsort($imageFiles);

        $returnData = [
            'folderName' => $fileNameString, 
            'folderPath' => $uploadPath,
            'imageFiles' => $imageFiles
        ];

        return $returnData;

    }

    // sampleonepagepdf

    public function info(Request $request) {
        phpinfo();
    }

    private function logRequest($request) {
        $req = new VisionRequest();
        $req->request_ip = request()->ip();
        $req->save();
    }
}