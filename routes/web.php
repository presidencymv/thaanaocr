<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ThaanaOCRController@index')->name('home');
Route::post('/upload', 'ThaanaOCRController@uploadPhoto');
Route::get('/dummy', 'ThaanaOCRController@dummy');
Route::get('/dummy/pdf', 'ThaanaOCRController@uploadPdf');

// Route::post('/uploadpdf', 'ThaanaOCRController@uploadPdf');

Route::get('/info', 'ThaanaOCRController@info');

Route::get('/annotate', 'AnnotationController@displayForm');
Route::post('/annotate', 'AnnotationController@annotateImage');

Route::post('/download', 'ThaanaOCRController@downloadPdf');


Route::get('test-bucket', 'PdfController@detect_pdf_gcs');
Route::post('/uploadpdf', 'PdfController@uploadPdfToGCS');


