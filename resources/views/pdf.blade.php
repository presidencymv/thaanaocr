<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>

<body class="dv faseyha p-3 ">
	<div class="p-3" style="direction: rtl!important; text-align: right;">

	{!! $text !!}
</div>
	</body>
</html>