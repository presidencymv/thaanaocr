<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body class="bg-info">
    <div id="app" class="mt-3">
        <OCR></OCR>
        
    </div>

    <div class="p-3 text-center text-dark">
        &copy; {{ date('Y') }} The President's Office
    </div>
    <input type="hidden" name="vision_count" value="{{$count}}"/>
</body>

<!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</html>

